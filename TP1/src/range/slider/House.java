package range.slider;


import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

public class House extends JPanel {
	
	public int price;
	public JLabel priceLabel;
	public String name;
	public JLabel nameLabel;
	public int nbRoom;
	public JLabel nbLabel;
	private static int sizeIcon = 60;
	
	public House(String name, int room, int price) {
		this.setLayout(new FlowLayout());
		this.price =price;
		this.nbRoom = room;
		this.name = name;
		
		String data[] = {"Name  : "+name,"Number of Room : "+nbRoom,"Price : "+price};
		JList<String> listInfo = new JList<String>(data);
		listInfo.setLayoutOrientation(JList.VERTICAL);
		listInfo.setBackground(this.getBackground());
		
		
		
		JLabel image = new JLabel();
		image.setIcon(new ImageIcon (
				(new ImageIcon("src/range/slider/home-icon-silhouette.png")
						.getImage().getScaledInstance(sizeIcon, sizeIcon, Image.SCALE_DEFAULT))));
		
		
		this.add(image);
		this.add(listInfo);
	}


}
