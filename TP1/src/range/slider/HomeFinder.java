package range.slider;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Scrollbar;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ScrollPaneLayout;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

public class HomeFinder extends JFrame implements Updater {

	public static int nbHouse = 40;
	public House houseList[] = new House[nbHouse];
	public int[] nbRoom = { 0, 10};
	public int[] price = { 0, 200000 };

	JPanel rangePan, housePan;
	JScrollPane scrollPan;
	RangeSlider sliderPrice, sliderRoom;

	public HomeFinder() {
		for (int i = 0; i < nbHouse; i++) {
			houseList[i] = (new House("House" + i, (int) (Math.random() * nbRoom[1]) + 1,
					(int) (Math.random() * price[1])));
		}
	}

	public void affichage() {
		housePan = new JPanel();
		housePan.setLayout(new BoxLayout(housePan, BoxLayout.Y_AXIS));

		rangePan = new JPanel();
		rangePan.setLayout(new BoxLayout(rangePan, BoxLayout.PAGE_AXIS));
		this.setLayout(new BorderLayout());

		// Paramètre de la fenêtre
		this.setTitle("HomeFinder");
		this.setSize(500, 800);
		setResizable(false);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		// Ajout du bouton à notre content pane
		sliderPrice = new RangeSlider(100, 15, 90, 300, 60, price[0], price[1], 2, this);
		sliderRoom = new RangeSlider(100, 15, 90, 300, 60, nbRoom[0], nbRoom[1], 1, this);
		JLabel titleSlider = new JLabel("Price");
		rangePan.add(titleSlider);
		rangePan.add(sliderPrice);
		JLabel titleSlider2 = new JLabel("Number of room");
		rangePan.add(titleSlider2);
		rangePan.add(sliderRoom);

		for (int i = 0; i < nbHouse; i++) {
			housePan.add(houseList[i]);
		}

		scrollPan = new JScrollPane(housePan, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPan.getVerticalScrollBar().setUnitIncrement(5);

		this.getContentPane().add(rangePan, BorderLayout.NORTH);
		this.getContentPane().add(scrollPan, BorderLayout.CENTER);

		this.setVisible(true);
	}

	public void update() {
		House h;
		housePan.removeAll();
		for (int i = 0; i < nbHouse; i++) {
			h = houseList[i];
			if (h.price >= sliderPrice.getValueLeft() && h.price <= sliderPrice.getValueRight()
					&& h.nbRoom >= sliderRoom.getValueLeft() && h.nbRoom <= sliderRoom.getValueRight()) {
				housePan.add(h);
			}
		}
		housePan.repaint();

	}

}
