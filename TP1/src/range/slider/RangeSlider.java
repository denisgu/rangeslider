package range.slider;

import java.awt.Color;


import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class RangeSlider extends JPanel {
	
	public int x, y, h, w, wSlider, hSlider, xSlider, ySlider;
	public Rectangle2D buttonLeft, buttonRight, backGround;
	private Model model;
	private Controler controler;
	public JLabel textButtonLeft, textButtonRight;
	
	public int paddingHorizontal;
	public int paddingVertical;
	public int sizeRect;

	public RangeSlider(int x, int y, int h, int w, int sizeRect, int minModel, int maxModel, int pasModel, Updater u) {
		this.model = new Model(minModel, maxModel, pasModel, u);
		this.controler = new Controler(model);
		this.addMouseListener(controler);
		this.addMouseMotionListener(controler);
		
		this.x = x;
		this.y = y;
		this.h = h;
		this.w = w;
		
		paddingHorizontal = sizeRect/2;
		
		this.hSlider = h/6;
		
		paddingVertical = (h-hSlider)/2;
		
		this.sizeRect = sizeRect;
		
		this.xSlider = x+paddingHorizontal;
		this.ySlider = paddingVertical;
		this.wSlider = w-paddingHorizontal;
		
		
		
		textButtonLeft =  new JLabel(""+model.valeurButtonLeft);
		textButtonRight = new JLabel(""+model.valeurButtonRight);
		
		this.add(textButtonLeft);
		this.add(textButtonRight);
		
		buttonRight = new Rectangle2D.Double(x+wSlider, 0, this.sizeRect, (h-hSlider)/2);
		buttonLeft = new Rectangle2D.Double(x, ySlider+hSlider, this.sizeRect, (h-hSlider)/2);
		backGround = new Rectangle2D.Double(xSlider, ySlider, wSlider, hSlider);
		


	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Color blue = new Color(100, 155, 201);
		Color grey = new Color(216, 215, 215);
		
		textButtonLeft.setBounds(buttonLeft.getBounds());
		textButtonRight.setBounds(buttonRight.getBounds());
		textButtonLeft.setHorizontalAlignment(JLabel.CENTER);
		textButtonLeft.setText(""+model.valeurButtonLeft);
		textButtonRight.setHorizontalAlignment(JLabel.CENTER);
		textButtonRight.setText(""+model.valeurButtonRight);
		
		
		//BackGround 
		((Graphics2D) g).setColor(grey);
		((Graphics2D) g).fill(backGround);
		
		//Range Selection 
		((Graphics2D) g).setColor(blue);
		((Graphics2D) g).fill(new Rectangle2D.Double(buttonLeft.getCenterX(), ySlider, (buttonRight.getCenterX() - buttonLeft.getCenterX()), hSlider));
		
		//Button Left
		((Graphics2D) g).setColor(Color.WHITE);
		((Graphics2D) g).fill(buttonLeft);
		
		//Button Right
		((Graphics2D) g).setColor(Color.WHITE);
		((Graphics2D) g).fill(buttonRight);
	}

	public Dimension getPreferredSize() {
		return new Dimension(100, 100);
	}
	
	//Getters of information on the slider.
	
	public int getValueLeft() {
		return model.getValeurButtonLeft();
	}
	
	public int getValueRight() {
		return model.getValeurButtonRight();
	}
	
	public int getPas() {
		return model.getPas();
	}

	public int getMax() {
		return model.getMax();
	}
	
	public int getMin() {
		return model.getMin();
	}
}