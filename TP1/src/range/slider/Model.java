package range.slider;


public class Model {
	
	protected int minimum;
	protected int maximum;
	protected int pas;
	protected int valeurButtonLeft;
	protected int valeurButtonRight;
	private Updater updater;
	
	public Model(int min, int max, int pas, Updater u) {
		this.minimum = min;
		this.maximum = max;
		this.pas = pas;
		this.valeurButtonLeft =min;
		this.valeurButtonRight = max;
		this.updater = u;
	}

	public int getValeurButtonLeft() {
		return valeurButtonLeft;
	}

	public void setValeurButtonLeft(int valeurA) {
		if(valeurA >= minimum && valeurA <= valeurButtonRight && valeurButtonLeft != valeurA ) {
			this.valeurButtonLeft = valeurA + valeurA%pas;
			updater.update();
		}
			
	}

	public int getValeurButtonRight() {
		return valeurButtonRight;
	}

	public void setValeurButtonRight(int valeurB) {
		if(valeurB <= maximum && valeurB >= valeurButtonLeft && valeurButtonRight != valeurB){
			this.valeurButtonRight = valeurB + valeurB%pas;
			updater.update();
		}
			
	}
	
	public int getPas() {
		return pas;
	}

	public int getMax() {
		return maximum;
	}
	
	public int getMin() {
		return minimum;
	}
	

}
