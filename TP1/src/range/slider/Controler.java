package range.slider;


import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;

public class Controler implements MouseInputListener{
	Point2D beginPoint;
	Point2D endPoint;
	private Boolean inDraggingLeft, inDraggingRight;
	private Model model;
	
	public Controler(Model m) {
		model = m;
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		endPoint = e.getPoint();
		double x = endPoint.getX();
		double y = endPoint.getY();
		RangeSlider slider = (RangeSlider) e.getSource();
		int pas = model.getPas();
		int max = model.getMax(); 
		try {
			if (inDraggingLeft && slider.buttonLeft.getMaxX() > x && slider.buttonLeft.getMinX() < x  && 
					slider.buttonRight.getCenterX() > x ) {
				slider.setCursor(new Cursor(Cursor.HAND_CURSOR));
				double widthTemp = slider.buttonLeft.getWidth();
				
				if(x >= (slider.x + (widthTemp/2)) && x <= (slider.x +slider.w )) {
					slider.buttonLeft.setRect(x-(widthTemp/2), slider.buttonLeft.getY(), widthTemp, slider.buttonLeft.getHeight());
				}
				
				//Actualisation du model avec prise en compte du pas
				double newValeur = (((x-slider.xSlider)/slider.wSlider)* max);
				int newAffichage = (int) newValeur;
				if((int)newValeur%pas == 0) {
					newAffichage = Math.round((float)newValeur);
				}else {
					if(Math.abs((newAffichage + newAffichage%pas)- newValeur) < Math.abs((newAffichage - newAffichage%pas)- newValeur)) {
						newAffichage = newAffichage + newAffichage%pas;
					}else {
						newAffichage = newAffichage - newAffichage%pas;
					}
				}
				model.setValeurButtonLeft(newAffichage);
				slider.repaint();
				beginPoint = endPoint;
			}
		} catch (Exception e1) {}
		try {
			if (inDraggingRight && slider.buttonRight.getMaxX() > x && slider.buttonRight.getMinX() < x  
				&& slider.buttonLeft.getCenterX() < x  ) {
				
				slider.setCursor(new Cursor(Cursor.HAND_CURSOR));
				double widthTemp = slider.buttonRight.getWidth();
				if(x >= slider.x  && x <= slider.x +slider.w ) {
					slider.buttonRight.setRect(x-(widthTemp/2), slider.buttonRight.getY(), widthTemp, slider.buttonRight.getHeight());
				}
				
				//Actualisation du model avec prise en compte du pas
				double newValeur = (((x-slider.xSlider)/slider.wSlider)* max);
				int newAffichage = (int) newValeur;
				if((int)newValeur%pas == 0) {
					newAffichage = Math.round((float)newValeur);
				}else {
					if(Math.abs((newAffichage + newAffichage%pas)- newValeur) < Math.abs((newAffichage - newAffichage%pas)- newValeur)) {
						newAffichage = newAffichage + newAffichage%pas;
					}else {
						newAffichage = newAffichage - newAffichage%pas;
					}
				}
				model.setValeurButtonRight(newAffichage);
				slider.repaint();
				beginPoint = endPoint;
			}
		} catch (Exception e2) {}
		

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		RangeSlider slider = (RangeSlider) e.getSource();
		beginPoint = e.getPoint();
		if(slider.buttonLeft.contains(beginPoint) || slider.buttonRight.contains(beginPoint)) {
			slider.setCursor(new Cursor(Cursor.HAND_CURSOR));
		}else {
			slider.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		RangeSlider slider = (RangeSlider) e.getSource();
		int nbClick = e.getClickCount();
		int pas = model.getPas();
		Point2D clickedPoint = e.getPoint();
		
		//IF DOUBLE CLICK BUTTON LEFT
		if(slider.buttonLeft.contains(clickedPoint) && nbClick >=2) {
			String input = JOptionPane.showInputDialog("Please enter value between "+model.getMin()+" and "+
					model.getValeurButtonRight());
			try {
				int in = Integer.parseInt(input);
				model.setValeurButtonLeft(in);
				slider.buttonLeft.setRect(slider.xSlider+(model.getValeurButtonLeft()/(float)model.getMax()*slider.w-(slider.buttonLeft.getWidth()/2)),
						slider.buttonLeft.getY(), slider.buttonLeft.getWidth(), slider.buttonLeft.getHeight());
				slider.repaint();
			} catch (Exception e2) {
				System.err.println("wrong integer");
			}
			
		//IF DOUBLE CLICK BUTTON RIGHT
		}else if(slider.buttonRight.contains(clickedPoint) && nbClick >=2) {
			String input = JOptionPane.showInputDialog("Enter value between "+model.getValeurButtonLeft()+" and "+
					model.getMax());
			try {
				int in = Integer.parseInt(input);
				model.setValeurButtonRight(in);
				slider.buttonRight.setRect(slider.xSlider+(((float)model.getValeurButtonRight()/(float)model.getMax())*slider.wSlider-(slider.buttonLeft.getWidth()/2)),
						slider.buttonRight.getY(), slider.buttonRight.getWidth(), slider.buttonRight.getHeight());
				slider.repaint();
			} catch (Exception e2) {
				System.err.println("wrong integer");
			}	
			
		
			// IF CLICKED LEFT
		}else if(slider.backGround.contains((Point)clickedPoint)&& clickedPoint.getX() < slider.buttonLeft.getCenterX()) {
			model.setValeurButtonLeft(model.getValeurButtonLeft()-pas);
			slider.buttonLeft.setRect(slider.xSlider+((float)model.getValeurButtonLeft()/(float)model.getMax()*slider.wSlider-(slider.buttonLeft.getWidth()/2)),
					slider.buttonLeft.getY(), slider.buttonLeft.getWidth(), slider.buttonLeft.getHeight());
			slider.repaint();
		// IF CLICKED RIGHT
		}else if(slider.backGround.contains((Point)clickedPoint)&& clickedPoint.getX() > slider.buttonRight.getCenterX()) {
			model.setValeurButtonRight(model.getValeurButtonRight()+pas);
			slider.buttonRight.setRect(slider.xSlider+((float)model.getValeurButtonRight()/(float)model.getMax()*slider.wSlider-(slider.buttonLeft.getWidth()/2)),
					slider.buttonRight.getY(), slider.buttonRight.getWidth(), slider.buttonRight.getHeight());
			slider.repaint();
		// IF CLICKED CENTER
		}else if(slider.backGround.contains((Point)clickedPoint)&& clickedPoint.getX() > slider.buttonLeft.getCenterX() 
				&& clickedPoint.getX() < slider.buttonRight.getCenterX()) {
			model.setValeurButtonLeft(model.getValeurButtonLeft()+pas);
			slider.buttonLeft.setRect(slider.xSlider+((float)model.getValeurButtonLeft()/(float)model.getMax()*slider.wSlider-(slider.buttonLeft.getWidth()/2)),
					slider.buttonLeft.getY(), slider.buttonLeft.getWidth(), slider.buttonLeft.getHeight());
			
			model.setValeurButtonRight(model.getValeurButtonRight()-pas);
			slider.buttonRight.setRect(slider.xSlider+((float)model.getValeurButtonRight()/(float)model.getMax()*slider.wSlider-(slider.buttonLeft.getWidth()/2)),
					slider.buttonRight.getY(), slider.buttonRight.getWidth(), slider.buttonRight.getHeight());
			slider.repaint();		}
		
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		RangeSlider slider = (RangeSlider) e.getSource();
		beginPoint = e.getPoint();
		if(slider.buttonLeft.contains(beginPoint)) {
			inDraggingLeft =true;
		}
		if(slider.buttonRight.contains(beginPoint)) {
			inDraggingRight = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		inDraggingLeft =false;
		inDraggingRight= false;

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

}
