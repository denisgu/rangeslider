# Compte rendu

## Fonctionnalité :

- Rangle slider : 
    - Vous pouvez bouger les boutons blancs pour pouvoir mettre à jour la valeur.
    - Vous pouvez double clicker sur un boutons blanc pour mettre une nouvelle valeur.
    - Vous pouvez clicker sur la barre sur le milieu pour reduire la zone du slider (valeur de gauche s'incrémente, la valeur de droite se décremente).
    - Vous pouvez clicker sur la barre a gauche du bouton gauche pour pouvoir décrémenter la valeur du bouton gauche.
    - Vous pouvez clicker sur la barre à droite du bouton droite pour pouvoir incrémenter la valeur du bouton droite.

- Home Finder : 

    - Vous pouvez chercher des maison avec les Range slider du dessus.
    