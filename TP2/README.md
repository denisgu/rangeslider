# Compte rendu
DENIS Guillaume
DE ARAUJO Bastien

## Fonctionnalité :

- Vous pouvez faire un clique droit sur la zone de "dessin" ce qui affichera le marking menu autour de votre souris.
Lorsque le marking Menu est affiché vous pouvez : (toutes les actions ci-dessous nécessite que le clique droit soit toujours enfoncé).
	- Choisir un outil en déplaçant votre souris sur l'outil voulu.
	L'outil selectionner se distingue avec une police plus grosse et en gras ainsi qu'une zone grisé. 
	- Si vous restez sur un outil pendant quelques secondes lorsque vous bougerez l'outil sera selectionné et un deuxième marking menu s'ouvrira afin que vous puissiez choisir la couleur utilisé par votre outil.
	- Vous pouvez aussi selectionner un outil et afficher le marking menu de choix de couleur en sortant du marking menu.
- Une fois un outil et une couleur selectionnée alors vous pouvez dessiner en faisant un clique gauche. 
- Vous pouvez nettoyer la page grâce au bouton clear se trouvant en haut à droite. 
- Vous pouvez activer et désactiver le mode expert grâce à la checkbox se trouvant en haut à gauche.
- Le mode expert, lorsque vous faites un clique droit les marking menu se lance mais ne s'affiche que au bout de quelques seconde dans le cas ou vous n'avez pas choisi d'outil ou de couleur avant ce temps. Il est donc possible de choisir un outil et sa couleur sans que le marking menu ne s'affiche. Mais si vous oublier ou se trouve l'élément que vous voulez il vous suffira d'attendre et alors il s'affichera comme dans le mode normal.