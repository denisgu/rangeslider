package marking.menu;


import java.util.Vector;


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import marking.managepanel.MainPanel;
import marking.managepanel.MyShape;

/* paint *******************************************************************/

class Paint extends JFrame {
	
	protected Vector<MyShape> shapes = new Vector<MyShape>();
	private Checkbox checkExpertMode;
	private Button clearButton;
	private MainPanel panel;

	
	public Paint(String title) {
		super(title);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(800, 600));
		
        checkExpertMode = new Checkbox("Expert Mode");  
        clearButton = new Button("Clear");
        
        JToolBar toolbar = new JToolBar();
        toolbar.add(checkExpertMode);
        toolbar.add(clearButton);
       
        panel = new MainPanel(shapes, checkExpertMode, clearButton);
        BorderLayout MainBorder = new BorderLayout();
        this.setLayout(MainBorder);
       
        JPanel panelTop = new JPanel();
        BorderLayout optionPanel = new BorderLayout();
        panelTop.setLayout(optionPanel);
        panelTop.add(toolbar);
       
        add(panelTop, BorderLayout.NORTH);
		add(panel, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}

	/* main *********************************************************************/

	public static void main(String argv[]) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Paint paint = new Paint("paint");
			}
		});
	}
}