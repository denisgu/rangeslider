package marking.managepanel;

import java.awt.Color;
import java.awt.Shape;

public class MyShape {
	private Shape shape;
	private Color color;
	
	public MyShape(Shape shape, Color color) {
		this.shape = shape;
		this.color = color;
	}

	public Shape getShape() {
		return shape;
	}

	public Color getColor() {
		return color;
	}	
}
