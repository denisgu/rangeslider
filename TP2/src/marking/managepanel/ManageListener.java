package marking.managepanel;

import static java.lang.Math.abs;

import static java.lang.Math.min;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.Vector;
import javax.swing.event.MouseInputListener;

import marking.pie.PieColor;
import marking.pie.PieTool;
import marking.pie.Tool;

public class ManageListener implements MouseInputListener {
	
	private Vector<MyShape> shapes;
	private Shape shapeTool;
	public static final int size = 100;
	protected Tool [] tools = {new Tool("pen"), new Tool("rect"), new Tool("ellipse")};
	private Tool tool;
	private Point o;
	private int buttonPressed;
	private PieTool pieTool;
	private PieColor pieColor;
	private MainPanel panel;
	protected long timeBegin, timeTool;
	protected boolean clicked;
	private Checkbox checkExpertMode;
	private Button clearButton;


	public ManageListener(MainPanel panel, Vector<MyShape> shapes2, PieColor c, PieTool t, Checkbox checkExpertMode, Button clearButton) {
		this.shapes = shapes2;
		this.panel = panel;
		this.pieTool = t;
		this.pieColor = c;
		this.checkExpertMode = checkExpertMode;
		this.clearButton = clearButton;
		clearButton.addMouseListener(this);
		checkExpertMode.addMouseListener(this);	
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(clearButton)) {
			shapes.removeAll(shapes);
		}
		if(e.getSource().equals(checkExpertMode)) {
			if(checkExpertMode.getState()) {
				panel.timePro = 1000;
			}else {
				panel.timePro = 0;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		clicked = true;
		buttonPressed = e.getButton();
		o = e.getPoint();
		if (e.getButton() == MouseEvent.BUTTON3) {
			timeBegin = System.currentTimeMillis();
			timeTool = System.currentTimeMillis();
			pieTool.display(o);
			panel.repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		clicked = false;
		shapeTool = null;
		pieColor.dispose();
		pieTool.dispose();
		panel.removeAll();
		pieTool.dispose();
		panel.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(buttonPressed == MouseEvent.BUTTON3) {
			Point2D point = new Point(e.getX(), e.getY());
			if(!pieColor.isDisplay() && pieTool.isInside(point) && System.currentTimeMillis() < timeTool + 1250 ) {
				Tool previousTool = tool;
				tool = pieTool.chooseTool(point);
				if(!tool.equals(previousTool)) {
					timeTool = System.currentTimeMillis();
				}
			}else {
				pieTool.dispose();
				// If you're not in pieTool then display pieColor
				if(!pieColor.isDisplay()) {
					timeBegin = System.currentTimeMillis();
					pieColor.display(point);
					panel.removeAll();
				}
				panel.repaint();
				if(pieColor.isInside(point)) {
					pieColor.chooseColor(point);
				}
			}
		}else if(buttonPressed == MouseEvent.BUTTON1) {
			if(tool != null) {
				switch (tool.getName()) {
					case "rect": //Work
						Rectangle2D.Double rect = (Rectangle2D.Double) shapeTool;
						if (rect == null) {
							rect = new Rectangle2D.Double(o.getX(), o.getY(), 0, 0);
							shapes.add(new MyShape(shapeTool = rect, pieColor.getCurrentColor()));
						}
						
						rect.setRect(min(e.getX(), o.getX()), min(e.getY(), o.getY()), abs(e.getX() - o.getX()),
								abs(e.getY() - o.getY()));
						break;
					case "pen": //Work
						Path2D.Double path = (Path2D.Double) shapeTool;
						if (path == null) {
							path = new Path2D.Double();
							path.moveTo(o.getX(), o.getY());
							shapes.add(new MyShape(shapeTool = path, pieColor.getCurrentColor()));
						}
						path.lineTo(e.getX(), e.getY());
						break;
					case "ellipse": //Work
						Ellipse2D.Double ell = (Ellipse2D.Double) shapeTool;
						if (ell == null) {
							ell = new Ellipse2D.Double(o.getX(), o.getY(), 0, 0);
							shapes.add(new MyShape(shapeTool = ell, pieColor.getCurrentColor()));
						}
						ell.setFrame((float) min(e.getX(), o.getX()), (float) min(e.getY(), o.getY()),
								(float) abs(e.getX() - o.getX()), (float) abs(e.getY() - o.getY()));
						break;
						
					case "arc": // work
						Arc2D.Double arc = (Arc2D.Double) shapeTool;
						if (arc == null) {
							arc = new Arc2D.Double(o.getX(), o.getY(), 0, 0, 0, 0, Arc2D.PIE);
							shapes.add(new MyShape(shapeTool = arc, pieColor.getCurrentColor()));
						}
						arc.setArc(arc.x, arc.y, (float) abs(e.getX() - o.getX()), (float) abs(e.getY() - o.getY()), 0, (e.getY() - o.getY()>0) ? 180 : -180 , Arc2D.PIE);
						break;
					
					case "cubicCurve"://work approximatively
						CubicCurve2D.Double cubic = (CubicCurve2D.Double) shapeTool;
						if (cubic == null) {
							cubic = new CubicCurve2D.Double(o.getX(), o.getY(), 10, 10, 50, 50, o.getX(), o.getY());
							shapes.add(new MyShape(shapeTool = cubic, pieColor.getCurrentColor()));
						}
						cubic.setCurve(cubic.x1, cubic.y1 ,
								400, 300,  0, 0, (float) e.getX(), (float) e.getY() );
						break;
						
					case "line": //Work
						Line2D.Double line = (Line2D.Double) shapeTool;
						if (line == null) {
							line = new Line2D.Double(o.getX(), o.getY(), 0, 0);
							shapes.add(new MyShape(shapeTool = line, pieColor.getCurrentColor()));
						}
						line.setLine(line.x1, line.y1,
								(float) e.getX(), (float) e.getY());
						break;
						
					case "quadCurve": //work approximatively
						QuadCurve2D.Double quad = (QuadCurve2D.Double) shapeTool;
						if (quad == null) {
							quad = new QuadCurve2D.Double(o.getX(), o.getY(), 0, 0, o.getX(), o.getY());
							shapes.add(new MyShape(shapeTool = quad, pieColor.getCurrentColor()));
						}
						quad.setCurve(quad.x1, quad.y1, 400, 300, (double) e.getX(), (double)e.getY());
						break;
						
					case "roundRectangle": //work approximatively
						RoundRectangle2D.Double round = (RoundRectangle2D.Double) shapeTool;
						if (round == null) {
							round = new RoundRectangle2D.Double(o.getX(), o.getY(), 0, 0, 30, 30);
							shapes.add(new MyShape(shapeTool = round, pieColor.getCurrentColor()));
						}
						round.setRoundRect(min(e.getX(), o.getX()), min(e.getY(), o.getY()), abs(e.getX() - o.getX()),
								abs(e.getY() - o.getY()),30,30);
						break;
						
					case "triangle": //work
						Polygon tri = (Polygon) shapeTool;
						if (tri == null) {
							tri = new Polygon();
							tri.addPoint((int)o.getX(),(int) o.getY());
							tri.addPoint((int)o.getX(),(int) o.getY());
							tri.addPoint((int)o.getX(),(int) o.getY());							
							shapes.add(new MyShape(shapeTool = tri, pieColor.getCurrentColor()));
						}
						int dist = (int) ( e.getY() - o.getY());
						int dist2 = (int) (dist*2/Math.sqrt(3));
						tri.xpoints[1] = (int) (o.getX() + Math.sin(Math.PI/6)* dist2)   ;
						tri.ypoints[1] = (int) (o.getY() + Math.cos(Math.PI/6)* dist2)  ;
						tri.xpoints[2] =  (int) (o.getX() - Math.sin(Math.PI/6)* dist2)  ;
						tri.ypoints[2] = (int) (o.getY() + Math.cos(Math.PI/6)* dist2) ;
								
						break;
				}
			}	
		}
		panel.removeAll();

		panel.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
