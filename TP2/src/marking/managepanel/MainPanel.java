package marking.managepanel;


import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Vector;
import javax.swing.JPanel;
import marking.pie.PieColor;
import marking.pie.PieTool;

public class MainPanel extends JPanel {
	
	protected Vector<MyShape> shapes = new Vector<MyShape>();
	private PieColor pieColor = new PieColor();
	private PieTool pieTool;
	private ManageListener manage;
	protected int timePro;

	public MainPanel(Vector<MyShape> shapes, Checkbox checkExpertMode, Button clearButton) {
		this.shapes = shapes;
		pieTool = new PieTool(this);
		manage = new ManageListener(this, shapes, pieColor, pieTool, checkExpertMode, clearButton);
		addMouseListener(manage);
		addMouseMotionListener(manage);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, getWidth(), getHeight());
		
		for (MyShape shape : shapes) {
			g2.setColor(shape.getColor());
			g2.draw(shape.getShape());
		}
		
		pieTool.paint(g2, manage.timeBegin, timePro, manage.clicked);
		pieColor.paint(g2, manage.timeBegin, timePro, manage.clicked);
		
		
	}
}
