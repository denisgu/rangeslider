package marking.pie;


import java.awt.geom.Point2D;

public class Tool {
	private String name;
	private Point2D position;

	public Tool(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public Point2D getPosition() {
		return position;
	}

	public void setPosition(Point2D position) {
		this.position = position;
	}

}
