package marking.pie;

import java.awt.Color;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Arc2D;
import javax.swing.JLabel;
import javax.swing.JTextField;
import marking.managepanel.MainPanel;

public class PieTool {

	private MainPanel panel;
	private Tool[] tools = { new Tool("pen"), new Tool("rect"), new Tool("ellipse"), new Tool("arc"), new Tool("cubicCurve"),
			new Tool("line"), new Tool("quadCurve"), new Tool("roundRectangle"), new Tool("triangle") };
	private int nbTools = tools.length;
	private Tool currentTool;
	private Point2D o;
	public static final int size = 100;
	int widthLabel = 120;
	int hightLabel = 20;

	private boolean display = false;

	public PieTool(MainPanel panel) {
		this.panel = panel;
	}

	public void paint(Graphics2D g, long timeBegin, int timePro, boolean clicked) {
		if (display) {
			if (System.currentTimeMillis() > timeBegin + timePro && clicked) {
				g.setColor(Color.LIGHT_GRAY);
				g.drawOval((int) o.getX() - (size / 2), (int) o.getY() - (size / 2), size, size);
			}
			int max = (8 < nbTools) ? 8 : nbTools;

			for (int i = 1; i < max + 1; i++) {
				JLabel text = new JLabel(tools[i - 1].getName());
				int xOffset = (int) ((size / 2) * Math.cos((2 * Math.PI) * i / max));
				int yOffset = (int) ((size / 2) * Math.sin((2 * Math.PI) * i / max));
				if (tools[i - 1] == currentTool) {
					text.setFont(new Font("Arial", Font.BOLD, 18));
					int xCenter = (int) o.getX();
					int yCenter = (int) o.getY();

					if (System.currentTimeMillis() > timeBegin + timePro && clicked)
						g.fill((Shape) new Arc2D.Double(xCenter - size / 2, yCenter - size / 2, size, size,
								-(360 / max) * (i - 1) - (180 / max), -360 / max, Arc2D.PIE));

				}
				tools[i - 1].setPosition(new Point((int) (o.getX() + xOffset), (int) (o.getY() + yOffset)));
				if (xOffset > 0) {
					yOffset -= hightLabel / 2;
					xOffset += 5;
				} else if (xOffset < 0) {
					xOffset -= widthLabel;
					yOffset -= hightLabel / 2;
				} else {
					if (yOffset < 0) {
						xOffset -= widthLabel / 2;
						yOffset -= hightLabel;
					} else {
						xOffset -= widthLabel / 2;
					}
				}
				text.setBounds((int) (o.getX() + xOffset), (int) (o.getY() + yOffset), widthLabel, hightLabel);
				text.setHorizontalAlignment(JTextField.CENTER);
				if (System.currentTimeMillis() > timeBegin + timePro && clicked)
					panel.add(text);
			}
			for (int i = 9; i <= nbTools; i++) {
				JLabel text = new JLabel(tools[i - 1].getName());
				int xOffset = (int) o.getX() - widthLabel / 2;
				int yOffset = (int) o.getY() + size / 2 + (i - 8) * hightLabel;
				text.setBounds((int) (xOffset), (int) (yOffset), widthLabel, hightLabel);
				text.setHorizontalAlignment(JTextField.CENTER);

				tools[i - 1]
						.setPosition(new Point(text.getX() + text.getWidth() / 2, text.getY() + text.getHeight() / 2));
				if (tools[i - 1] == currentTool) {
					text.setFont(new Font("Arial", Font.BOLD, 18));
				}
				text.setBackground(Color.BLACK);
				if (System.currentTimeMillis() > timeBegin + timePro && clicked)
					panel.add(text);
			}
		}

	}

	public Tool chooseTool(Point2D closer) {
		Tool closerTool = tools[0];
		double minDistance = closer.distance(tools[0].getPosition());
		double currentDistance;
		for (Tool currentTool : tools) {
			currentDistance = closer.distance(currentTool.getPosition());
			if (currentDistance < minDistance) {
				closerTool = currentTool;
				minDistance = currentDistance;
			}
		}
		this.currentTool = closerTool;
		return closerTool;
	}

	public boolean isInside(Point2D closer) {
		if ((closer.distance(o) > size / 2 + 10 || !display)) {
			if (!isInsideMoreTools(closer)) {
				return false;
			}
		}
		return true;
	}

	private boolean isInsideMoreTools(Point2D closer) {
		int currentY = (int) closer.getY();
		int currentX = (int) closer.getX();
		Point2D lastToolPoint = tools[nbTools - 1].getPosition();
		if (currentY < lastToolPoint.getY() + hightLabel && currentY > o.getY()+ size/2 && currentX > o.getX() - widthLabel / 4
				&& currentX < o.getX() + widthLabel / 4) {
			return true;
		}
		return false;
	}

	public void display(Point2D point) {
		display = true;
		o = point;
	}

	public void dispose() {
		display = false;
	}

}
