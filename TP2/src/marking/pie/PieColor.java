package marking.pie;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class PieColor {
	private Color[] colors = { Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED,
			Color.MAGENTA, Color.PINK, Color.BLACK, Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY };
	private int nbColor = colors.length;
	private ArrayList<Shape> polygons = new ArrayList<Shape>();
	private Color currentColor = Color.BLACK;
	private boolean display;
	public static final int size = 100;
	private Point2D beginPoint;

	public PieColor() {
		display = false;
	}

	public void paint(Graphics2D g, long timeBegin, int timePro, boolean clicked) {
		if (display) {
			polygons.removeAll(polygons);
			int xCenter = (int) beginPoint.getX();
			int yCenter = (int) beginPoint.getY();
			int max = (nbColor > 8) ? 8 : nbColor;
			for (int i = 1; i <= max; i++) {

				g.setColor(colors[i - 1]);

				Arc2D.Double a;
				if (currentColor != null && currentColor.equals(colors[i - 1])) {
					a = new Arc2D.Double(xCenter - size / (4.0 / 3.0), yCenter - size / (4.0 / 3.0), size * 1.5,
							size * 1.5, -(360 / max) * (i - 1) - (180 / max), -360 / max, Arc2D.PIE);
				} else {
					a = new Arc2D.Double(xCenter - size / 2, yCenter - size / 2, size, size,
							-(360 / max) * (i - 1) - (180 / max), -360 / max, Arc2D.PIE);
				}

				polygons.add(a);
				if (System.currentTimeMillis() > timeBegin + timePro && clicked)
					g.fill((Shape) a);

			}
			for (int i = max + 1; i <= nbColor; i++) {
				g.setColor(colors[i - 1]);
				int radius = 30;

				Ellipse2D.Double a;
				if (currentColor != null && currentColor.equals(colors[i - 1])) {
					a = new Ellipse2D.Double(xCenter - (radius + 10) / 2, yCenter + (radius) * 1.5 * (i - 7) - 10,
							radius + 10, radius + 10);
				} else {
					a = new Ellipse2D.Double(xCenter - radius / 2, yCenter + (radius * 1.5) * (i - 7), radius, radius);
				}

				polygons.add(a);
				if (System.currentTimeMillis() > timeBegin + timePro && clicked) {
					g.fill((Shape) a);
				}

			}
		}

	}

	public Color chooseColor(Point2D closer) {
		for (int i = 0; i < nbColor; i++) {
			if (polygons.isEmpty()) {
				return null;
			}
			if (polygons.get(i).contains(closer)) {
				currentColor = colors[i];
				return colors[i];
			}
		}
		return null;
	}

	public boolean isInside(Point2D closer) {
		for (int i = 0; i < nbColor; i++) {
			if (polygons.isEmpty()) {
				return false;
			}
			if (polygons.get(i).contains(closer)) {
				return true;
			}
		}
		return false;
	}

	public void display(Point2D beginpoint) {
		display = true;
		this.beginPoint = beginpoint;
	}

	public void dispose() {
		display = false;
	}

	public boolean isDisplay() {
		return display;
	}

	public Color getCurrentColor() {
		return currentColor;
	}

	public void setCurrentColor(Color currentColor) {
		this.currentColor = currentColor;
	}
}
